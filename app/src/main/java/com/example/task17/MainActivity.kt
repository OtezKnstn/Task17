package com.example.task17


import android.os.Bundle
//import android.support.v4.app.AppCompatActivity
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.task17.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        super.onCreate(savedInstanceState)
        binding.addCircle.setOnClickListener {
            binding.canvas.addCircle()
        }

        binding.addSquare.setOnClickListener {
            binding.canvas.addSquare()
        }

        binding.addRect.setOnClickListener {
            binding.canvas.addRectangle()
        }

        binding.resetCanvas.setOnClickListener{
            binding.canvas.reset()
            Toast.makeText(this, "Reset successfully", Toast.LENGTH_LONG).show()
        }

    }
}